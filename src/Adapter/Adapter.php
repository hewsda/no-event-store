<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Adapter;

use Hewsda\NoEventStore\Stream\Stream;
use Hewsda\NoEventStore\Stream\StreamName;

interface Adapter
{
    public function create(Stream $stream): void;

    public function load(StreamName $streamName): ?Stream;
}