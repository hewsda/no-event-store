<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Plugin;

use Hewsda\NoEventStore\EventStore;

interface Plugin
{
    public function setUp(EventStore $eventStore): void;
}