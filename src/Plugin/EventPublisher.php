<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Plugin;

use Hewsda\NoEventStore\EventStore;
use Prooph\Common\Event\ActionEvent;

class EventPublisher implements Plugin
{
    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * EventPublisher constructor.
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function setUp(EventStore $eventStore): void
    {
        $eventStore->getEmitter()->attachListener('commit.post', [$this, 'onEventStoreCommitPost']);
    }

    public function onEventStoreCommitPost(ActionEvent $actionEvent): void
    {
        /** @var array $recordedEvents */
        $recordedEvents = $actionEvent->getParam('recordedEvents', new \ArrayIterator());

        foreach ($recordedEvents as $recordedEvent) {
            $this->eventBus->dispatch($recordedEvent);
        }
    }
}