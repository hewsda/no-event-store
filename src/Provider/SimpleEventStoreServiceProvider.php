<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Provider;

use Hewsda\NoEventStore\CacheAdapter\EventStoreCacheAdapter;
use Hewsda\NoEventStore\EventStore;
use Hewsda\NoEventStore\Plugin\EventPublisher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Prooph\Common\Event\ProophActionEventEmitter;

class SimpleEventStoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerEventStorePlugin();
    }

    public function register()
    {
        $this->registerEventStore();
    }

    private function registerEventStore()
    {
        $this->app->singleton(EventStore::class, function (Application $app) {
            return new EventStore(
                $app->make(EventStoreCacheAdapter::class),
                new ProophActionEventEmitter()
            );
        });
    }

    private function registerEventStorePlugin()
    {
        $this->app->bind(EventPublisher::class);

        $this->app[EventPublisher::class]->setUp($this->app[EventStore::class]);
    }
}