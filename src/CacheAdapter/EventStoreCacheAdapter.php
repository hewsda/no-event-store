<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\CacheAdapter;

use Hewsda\NoEventStore\Adapter\Adapter;
use Hewsda\NoEventStore\Adapter\CanHandleTransaction;
use Hewsda\NoEventStore\Stream\Stream;
use Hewsda\NoEventStore\Stream\StreamName;
use Illuminate\Contracts\Cache\Repository;

class EventStoreCacheAdapter implements Adapter, CanHandleTransaction
{

    /**
     * @var [\Iterator]
     */
    private $streams = [];

    /**
     * @var Repository
     */
    private $store;

    /**
     * EventStoreCacheAdapter constructor.
     *
     * @param Repository $store
     */
    public function __construct(Repository $store)
    {
        $this->store = $store;
    }

    public function create(Stream $stream): void
    {
        $streamEvents = $stream->streamEvents();

        $streamEvents->rewind();

        $this->streams[$stream->streamName()->toString()] = $streamEvents;
    }

    public function load(StreamName $streamName): ?Stream
    {
        if ($this->store->has($streamName->toString())) {
            $streamEvents = $this->store->get($streamName->toString());

            $streamEvents->rewind();

            return new Stream($streamName, $streamEvents);
        }

        return null;
    }

    public function commit(): void
    {
        if (!$this->streams) {
            return;
        }

        $streamName = key($this->streams);

        /** @var \ArrayIterator $events */
        $events = $this->store->get($streamName);

        if (!$events) {
            $this->store->forever($streamName, $this->streams[$streamName]);

            $this->streams = [];

            return;
        }

        $events->append(current($this->streams[$streamName]));

        $this->store->forever(key($this->streams), $events);

        $this->streams = [];
    }

    public function rollback(): void
    {
        $this->streams = [];
    }

    public function beginTransaction(): void
    {
    }
}