<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Exception;

interface EventStoreException
{
}