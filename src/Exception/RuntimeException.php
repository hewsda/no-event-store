<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Exception;

class RuntimeException extends \RuntimeException implements EventStoreException
{
}