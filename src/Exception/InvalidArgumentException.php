<?php

declare(strict_types=1);

namespace Hewsda\NoEventStore\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements EventStoreException
{
}